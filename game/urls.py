#!/usr/bin/env python
#-*- coding:utf-8 -*-
from django.conf.urls.defaults import patterns, url
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',	
	url(r'^init_data/', 'game.views.init_data', name='init_data'),
    # url(r'^socialward/', include('socialward.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
