from django.db import models
from game.player.models import Player

# Create your models here.
class Realm(models.Model):
	player_id = models.ForeignKey(Player, db_column='player_id')
	# TODO - realm_name_generator()
	name = models.CharField(max_length=128)

class Area(models.Model):
	realm_id = models.ForeignKey(Realm, 
								db_column='realm_id', 
								related_name='areas')
	size = models.IntegerField()

class Spot(models.Model):
	area_id = models.ForeignKey(Area, db_column='area_id')
	
class ActionHistory(models.Model):
	spot_id = models.ForeignKey(Spot, db_column='spot_id')
	player_id = models.ForeignKey(Player, db_column='player_id')
	action = models.IntegerField()