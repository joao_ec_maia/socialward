from django.http import HttpResponse
from django.utils.simplejson import dumps

# Create your views here.
def init_data(request):	
	return HttpResponse(dumps({'success': True}), mimetype="application/x-json")